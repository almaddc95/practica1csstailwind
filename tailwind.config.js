/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  theme: {
    extend: {
      backgroundOpacity: {
        '98': '0.98',
       },
      colors: {
        green: '#C1E26D',
        white: '#FFFFFF',
        grey: '#333333',
        darkgrey: '#1F1F1F',
        offblack: '#0D0D0D',
      },
      fontFamily: {
        inter: ['Inter', 'sans-serif'],
      },
    },
  },
  plugins: [],
}

